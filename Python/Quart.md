# Quart

![image-20200323152934704](Quart.assets/image-20200323152934704.png)

Quart 是一个 Python ASGI 的 web 微框架，它旨在提供最简单的使用方式在 web 环境中使用 asyncio 功能，特别是已经存在的 Flask 应用。因为 Quart 是 Flask API 的超集因此这是可以做到的。

## 1. 包装器的使用

我自己定义了一个 `@login_required` 的包装器，用来做用户身份验证，我是从 Flask 框架进行迁移的，在 Flask 框架的函数定义如下：

```python
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        token = get_token()

        redis_db = db.get_redis_db()

        if not redis_db.exists(db.LOGIN_INFO_REDIS_KEY.format(token)):
            return jsonify({
                "code": 1,
                "message": "Not logged in"
            })

        redis_db.expire(db.LOGIN_INFO_REDIS_KEY.format(token), 30 * 60)
        return view(**kwargs)

    return wrapped_view
```

但是我在迁移到 Quart 框架的时候该函数无法运行，这是因为 Quart 是一个异步的框架，它的使用如下：

```python
from quart import Quart, websocket

app = Quart(__name__)

@app.route('/')
async def hello():
    return 'hello'

@app.websocket('/ws')
async def ws():
    while True:
        await websocket.send('hello')

app.run()

```

因此在包装其中也需要返回一个异步函数：

```python
def login_required(view):
    @functools.wraps(view)
    async def wrapped_view(**kwargs):
        token = get_token()

        redis_db = db.get_redis_db()

        if not redis_db.exists(db.LOGIN_INFO_REDIS_KEY.format(token)):
            return jsonify({
                "code": 1,
                "message": "Not logged in"
            })

        redis_db.expire(db.LOGIN_INFO_REDIS_KEY.format(token), 30 * 60)
        return await view(**kwargs)

    return wrapped_view
```

这是因为其路由端点函数必须是一个异步函数。

