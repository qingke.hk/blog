# Flask 中遇到的问题

[TOC]

## 1.Seesion 存储问题
Seesion 有两种存储方式：

1. Server side session：

   将 Seesion 存储在服务器端，使用 Session id 作为唯一标识符，然后将 Session id 放在 Cookie 中，这样每次请求的时候客户端都会携带该 Session id ，从而查找存储在服务端的 Seesion 的内容。

2. Client side session

   将 Session 加密之后存储在 Cookie 中，每次客户端请求的时候都会携带该加密内容，然后在服务端进行解密，从而使用 Session 。

Flask 默认使用 Client side session 的方式，通过配置中的 **SECRET_KEY** 进行加密，然后存储在 Cookie 之中，保存在客户端：

```python
from flask import Flask
app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
    SECRET_KEY="dev"
)
```

但是 Flask 提供 **Session Interface **接口，可以实现不同的存储方案。

![](Flask.assets/image-20200319104339982.png)

根据文档的描述可以看到 **Session Interface** 提供了一种简单的方式去替换 Flask 中的 Session，基于此接口的 Session 存储方案具体的可以参考 Flask 的扩展 [**Flask-Session**](https://pythonhosted.org/Flask-Session/) 。

## 2. `Working outside of application context.` 异常

**原因**：在 Application 没有完成初始化之前使用 `g` 变量，导致出现该异常，具体的该问题产生的原因文档中进行了说明，并提供解决方案：

![image-20200320161703081](Flask.assets/image-20200320161703081.png)

## 3. Flask `teardown_appcontext` 函数

文档中的解释是注册一个函数当 Apllcation 上下文结束的时候进行调用，同时在一次请求结束的时候也会调用。

![image-20200320162319025](Flask.assets/image-20200320162319025.png)

可以利用该函数在一次求情结束的时候关闭数据库的资源。

## 4. Flask 与 Asyncio 库

Flask 无法与 asyncio 比标准库一起使用，或者是我的水平不够 😂，但是有一个兼容 Flask API 的异步 Web 框架 Quart ，非常容易进行迁移。

![image-20200323150853908](Flask.assets/image-20200323150853908.png)

![image-20200323150929073](Flask.assets/image-20200323150929073.png)